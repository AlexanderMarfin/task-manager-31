package ru.tsc.marfin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.api.repository.IRepository;
import ru.tsc.marfin.tm.enumerated.Sort;
import ru.tsc.marfin.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @NotNull
    List<M> findAll(@Nullable Sort sort);

}
