package ru.tsc.marfin.tm.api.service;

import ru.tsc.marfin.tm.api.repository.IUserOwnedRepository;
import ru.tsc.marfin.tm.enumerated.Sort;
import ru.tsc.marfin.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

}
